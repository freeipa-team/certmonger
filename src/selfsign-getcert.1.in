.TH CERTMONGER 1 "November 3, 2009" "certmonger Manual"

.SH NAME
selfsign\-getcert

.SH SYNOPSIS
 selfsign\-getcert request [options]
 selfsign\-getcert resubmit [options]
 selfsign\-getcert start\-tracking [options]
 selfsign\-getcert status [options]
 selfsign\-getcert stop\-tracking [options]
 selfsign\-getcert list [options]
 selfsign\-getcert list\-cas [options]
 selfsign\-getcert refresh\-cas [options]

.SH DESCRIPTION
The \fIselfsign\-getcert\fR tool issues requests to a @CM_DBUS_NAME@
service on behalf of the invoking user.  It can ask the service to begin
enrollment, optionally generating a key pair to use, it can ask the
service to begin monitoring a certificate in a specified location for
expiration, and optionally to refresh it when expiration nears, it can
list the set of certificates that the service is already monitoring, or
it can list the set of CAs that the service is capable of using.

If no command is given as the first command\-line argument,
\fIselfsign\-getcert\fR will print short usage information for each of
its functions.

The \fIselfsign\-getcert\fR tool behaves identically to the generic
\fIgetcert\fR tool when it is used with the \fB\-c
\fI@CM_SELF_SIGN_CA_NAME@\fR option.

\fBcertmonger\fR's self\-signer doesn't use root certificates.  While the
\fB\-F\fR and \fB\-a\fR options will still be recognized, they will effectively
be ignored.

.SH BUGS
Please file tickets for any that you find at https://fedorahosted.org/certmonger/

.SH SEE ALSO
\fBcertmonger\fR(8)
\fBgetcert\fR(1)
\fBgetcert\-add\-ca\fR(1)
\fBgetcert\-add\-scep\-ca\fR(1)
\fBgetcert\-list\-cas\fR(1)
\fBgetcert\-list\fR(1)
\fBgetcert\-modify\-ca\fR(1)
\fBgetcert\-refresh\-ca\fR(1)
\fBgetcert\-refresh\fR(1)
\fBgetcert\-rekey\fR(1)
\fBgetcert\-remove\-ca\fR(1)
\fBgetcert\-request\fR(1)
\fBgetcert\-resubmit\fR(1)
\fBgetcert\-start\-tracking\fR(1)
\fBgetcert\-status\fR(1)
\fBgetcert\-stop\-tracking\fR(1)
\fBcertmonger\-certmaster\-submit\fR(8)
\fBcertmonger\-dogtag\-ipa\-renew\-agent\-submit\fR(8)
\fBcertmonger\-dogtag\-submit\fR(8)
\fBcertmonger\-ipa\-submit\fR(8)
\fBcertmonger\-local\-submit\fR(8)
\fBcertmonger\-scep\-submit\fR(8)
\fBcertmonger_selinux\fR(8)
